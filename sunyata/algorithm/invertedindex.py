"""
Usage:

docs = [
    "The quick brown fox",
    "Jumps over the lazy dog",
    "The quick dog outpaces the fox"
]

ii = InvertedIndex(docs)
docids = ii.findDoc("fox")
print(docids)
"""

class InvertedIndex:

    def __init__(self, documents = []):
        self.wordDocHash = {}
        self._setupIndex(documents)

    def _setupIndex(self, documents):
        for docId, text in enumerate(documents):
            words = text.split(' ')
            for word in words:
                if word not in self.wordDocHash:
                    self.wordDocHash[word] = []
                self.wordDocHash[word].append(docId)

    def findDoc(self, word):
        if word not in self.wordDocHash:
            return []
        return self.wordDocHash[word]

