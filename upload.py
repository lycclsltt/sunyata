"""
PYPI publish
"""

import sys
import subprocess

binExec = "python3"

cmdList = [
    "rm -rf ./sunyata.egg-info", "rm -rf ./dist",
    "%s setup.py sdist" % binExec,
    #"%s -m twine upload -u '%s' -p '%s' dist/*" %
    "%s -m twine upload --repository pypi dist/* --verbose" % (binExec),
    "rm -rf ./sunyata.egg-info",
    "rm -rf ./dist"
]

for cmd in cmdList:
    print(cmd)
    _, output = subprocess.getstatusoutput(cmd)
    print(output)